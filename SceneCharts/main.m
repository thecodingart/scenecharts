//
//  main.m
//  SceneCharts
//
//  Created by Brandon Levasseur on 2/13/15.
//  Copyright (c) 2015 TheCodingArt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
