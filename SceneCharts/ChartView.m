//
//  ChartView.m
//  SceneCharts
//
//  Created by Brandon Levasseur on 2/13/15.
//  Copyright (c) 2015 TheCodingArt. All rights reserved.
//

#import "ChartView.h"

#define ARC4RANDOM_MAX      0xFFFFFFFFu

const NSTimeInterval baseAnimationDelay = 1.0;

@interface ChartView()

@property (weak, nonatomic) SCNNode *chartNode;

@end

@implementation ChartView

- (void)awakeFromNib
{
    SCNScene *scene = [SCNScene scene];
    self.scene = scene;
    
    SCNNode *cameraNode = [SCNNode node];
    cameraNode.camera = [SCNCamera camera];
    cameraNode.position = SCNVector3Make(0, 20, 40);
    cameraNode.rotation = SCNVector4Make(1, 0, 0, -atan2(20.0, 40.0));
    [scene.rootNode addChildNode:cameraNode];
    SCNLight *directional = [SCNLight light];
    directional.type = SCNLightTypeDirectional;
    directional.color = [NSColor colorWithCalibratedWhite:0.3 alpha:1.0];
    
    SCNNode *directionalNode = [SCNNode node];
    directionalNode.light = directional;
    directionalNode.rotation = SCNVector4Make(0, 1, 0, -M_PI_4);
    
    [scene.rootNode addChildNode:directionalNode];
    
    SCNLight *ambient = [SCNLight light];
    ambient.type = SCNLightTypeAmbient;
    ambient.color = [NSColor colorWithCalibratedWhite:0.25 alpha:1.0];
    
    SCNNode *ambientNode = [SCNNode node];
    ambientNode.light = ambient;
    
    [scene.rootNode addChildNode:ambientNode];
    
    SCNLight *spotlight = [SCNLight light];
    spotlight.type = SCNLightTypeSpot;
    spotlight.color = [NSColor colorWithCalibratedWhite:0.4 alpha:1.0];
    
    SCNNode *spotlightNode = [SCNNode node];
    spotlightNode.light = spotlight;
    spotlightNode.position = SCNVector3Make(-30, 25, 30);
    
    spotlight.spotInnerAngle = 60;
    spotlight.spotOuterAngle = 100;
    spotlight.castsShadow = YES;
    
    SCNNode *target = scene.rootNode;
    spotlightNode.constraints = @[[SCNLookAtConstraint lookAtConstraintWithTarget:target]]; //Always looks at the center of the scene
    
    SCNFloor *floor = [SCNFloor floor];
    
    //A solid white color, not affect by light
    floor.firstMaterial.diffuse.contents = [NSColor whiteColor];
    floor.firstMaterial.lightingModelName = SCNLightingModelConstant;
    
    //Less reflective and decrease by distance
    floor.reflectivity = 0.15;
    floor.reflectionFalloffEnd = 15;
    
    SCNNode *floorNode = [SCNNode nodeWithGeometry:floor];
    [scene.rootNode addChildNode:floorNode];
    
    [[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(generateNewNumbers) userInfo:nil repeats:YES] fire];
}

- (void)setNumbers:(NSArray *)numbers
{
    
    [self.chartNode enumerateChildNodesUsingBlock:^(SCNNode *child, BOOL *stop) {
        if ([child.name isEqualToString:@"cylinder"]) {
            [self shrinkingCylinderAnimationWithDelay:0.0 forNode:child];
        } else if ([child.name isEqualToString:@"text"]) {
            [self shrinkTextAnimationWithDelay:0.0 forNode:child];
        }
    }];
    
    [self reverseRotationAnimationWithDelay:0.0 forNode:self.chartNode];
    [self performSelector:@selector(generateNewChartForNumbers:) withObject:numbers afterDelay:1.5];
    

}

- (void)generateNewChartForNumbers:(NSArray *)numbers
{
    [self.chartNode removeFromParentNode];
    
    SCNNode *chartNode = [SCNNode node];
    chartNode.name = @"chart";
    [self.scene.rootNode addChildNode:chartNode];
    chartNode.rotation = SCNVector4Make(0, 1, 0, -M_PI/8.0);
    
    self.chartNode = chartNode;
    
    const CGFloat barRadius = 2.3;
    const CGFloat margin = 1.5;
    
    const CGFloat stepLength = barRadius * 2.0 + margin;
    
    //one node to hold all the numbers inside the chart node
    SCNNode *numbersNode = [SCNNode node];
    
    [chartNode addChildNode:numbersNode];
    
    
    SCNMaterial *cylinderMaterial = [self cylinderMaterial];
    
    NSNumberFormatter *spellOutFormatter = [NSNumberFormatter new];
    spellOutFormatter.numberStyle = NSNumberFormatterSpellOutStyle;
    
    [numbers enumerateObjectsUsingBlock:^(NSNumber *number, NSUInteger idx, BOOL *stop) {
        
        //create one node to position both the cylinder and the text along the x-axis
        SCNNode *barNode = [SCNNode node];
        barNode.position = SCNVector3Make(stepLength * idx, 0, 0);
        barNode.name = @"bar";
        [numbersNode addChildNode:barNode];
        
        CGFloat height = number.doubleValue;
        SCNCylinder *cylinder = [SCNCylinder cylinderWithRadius:barRadius height:height];
        
        cylinder.firstMaterial = cylinderMaterial;
        SCNNode *cylinderNode = [SCNNode nodeWithGeometry:cylinder];
        cylinderNode.position = SCNVector3Make(0, height/2, 0);
        [barNode addChildNode:cylinderNode];
        
        
        //create a text element to display on top of each bar
        NSString *numberText = [spellOutFormatter stringFromNumber: number];
        SCNText *text = [SCNText textWithString:numberText extrusionDepth:0.2];
        
        text.firstMaterial.diffuse.contents = [NSColor colorWithCalibratedWhite:0.9 alpha:1.0];
        text.font = [NSFont systemFontOfSize:2.5];
        text.flatness = 0.1; //A lower value for smoother text
        
        SCNNode *textNode = [SCNNode nodeWithGeometry:text];
        textNode.transform = SCNMatrix4Invert(chartNode.worldTransform); //Insures previous transformations are reversed and the text faces the screen
        
        //Center on top of the bar
        textNode.position = SCNVector3Make(-text.textSize.width/2, height, 0);
        
        [barNode addChildNode:textNode];
        
        
        NSTimeInterval animationDelay = baseAnimationDelay + 0.5 * idx / (numbers.count + 1.0);
        [cylinderNode addAnimation:[self growingCylinderAnimationWithDelay:animationDelay] forKey:@"grow"];
        cylinderNode.name = @"cylinder";
        [textNode addAnimation:[self growTextAnimationWithDelay:animationDelay] forKey:@"move text upwards"];
        textNode.name = @"text";
        
    }];
    //Center the numbers in the chart
    SCNVector3 boundingBoxMin;
    SCNVector3 boundingBoxMax;
    
    [numbersNode getBoundingBoxMin:&boundingBoxMin max:&boundingBoxMax];
    
    CGFloat totalWidth = boundingBoxMax.x - boundingBoxMin.x;
    CGFloat middleX = boundingBoxMin.x - totalWidth/2.0;
    
    numbersNode.position = SCNVector3Make(middleX, 0, 0);
    
    [chartNode addAnimation:[self chartRotationAnimationWithDelay:baseAnimationDelay] forKey:@"Rotate the entire chart"];

}

- (SCNMaterial *)cylinderMaterial
{
    SCNMaterial *material = [SCNMaterial material];
    
    NSColor *lightBlueColor = [NSColor colorWithCalibratedRed:74.0/255.0 green:165.0/255.0 blue:227.0/255.0 alpha:1.0];
    
    material.diffuse.contents = lightBlueColor;
    material.specular.contents = [NSColor whiteColor];
    material.shininess = 0.15;
    material.locksAmbientWithDiffuse = YES;
    
    return material;
}

- (CAAnimation *)growingCylinderAnimationWithDelay:(NSTimeInterval)delay
{
    CABasicAnimation *grow = [CABasicAnimation animationWithKeyPath:@"geometry.height"];
    grow.fromValue = @0.25;
    CABasicAnimation *move = [CABasicAnimation animationWithKeyPath:@"position.y"];
    move.fromValue = @0; //Makes it grow from the bottom as the position is in the center
    
    CAAnimationGroup *growGroup = [CAAnimationGroup animation];
    growGroup.animations = @[grow, move];
    growGroup.duration = 1.0;
    growGroup.beginTime = CACurrentMediaTime() + delay;
    growGroup.fillMode = kCAFillModeBackwards;
    growGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    return growGroup;
}

- (CAAnimation *)shrinkingCylinderAnimationWithDelay:(NSTimeInterval)delay forNode:(SCNNode *)node
{
    //SCNCylindar has a height property in it's geometry class
    CABasicAnimation *shrinkAnimation = [CABasicAnimation animationWithKeyPath:@"geometry.height"];
    shrinkAnimation.fromValue = @([[node.geometry valueForKey:@"height"] doubleValue]);
    shrinkAnimation.toValue = @0;
    CABasicAnimation *moveAnimation = [CABasicAnimation animationWithKeyPath:@"position.y"];
    moveAnimation.fromValue = @(node.position.y);
    moveAnimation.toValue = @0;
    
    CAAnimationGroup *shrinkAnimationGroup = [CAAnimationGroup animation];
    shrinkAnimationGroup.animations = @[shrinkAnimation, moveAnimation];
    shrinkAnimationGroup.duration = 1.0;
    shrinkAnimationGroup.beginTime = CACurrentMediaTime() + delay;
    shrinkAnimationGroup.fillMode = kCAFillModeBackwards;
    shrinkAnimationGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    [node setValue:@0 forKeyPath:@"geometry.height"];
    node.position = SCNVector3Make(node.position.x, 0, node.position.z);
    [node addAnimation:shrinkAnimationGroup forKey:@"shrink animation group"];
    
    return shrinkAnimationGroup;
}

- (CAAnimation *)growTextAnimationWithDelay:(NSTimeInterval)delay
{
    //As the cylinder grows, the text moves upwards to it's final position
    CABasicAnimation *moveText = [CABasicAnimation animationWithKeyPath:@"position.y"];
    moveText.fromValue = @0;
    moveText.duration = 1.0;
    moveText.beginTime = CACurrentMediaTime() + delay;
    moveText.fillMode = kCAFillModeBackwards;
    moveText.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    return moveText;
}

- (CAAnimation *)shrinkTextAnimationWithDelay:(NSTimeInterval)delay forNode:(SCNNode *)node
{
    CABasicAnimation *moveTextAnimation = [CABasicAnimation animationWithKeyPath:@"position.y"];
    moveTextAnimation.fromValue = @(node.position.y);
    moveTextAnimation.toValue = @0;
    moveTextAnimation.duration = 1.0;
    moveTextAnimation.beginTime = CACurrentMediaTime() + delay;
    moveTextAnimation.fillMode = kCAFillModeBackwards;
    
    moveTextAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    node.position = SCNVector3Make(node.position.x, 0, node.position.z);
    [node addAnimation:moveTextAnimation forKey:@"shrink text animation"];
    return moveTextAnimation;
}

- (CAAnimation *)chartRotationAnimationWithDelay:(NSTimeInterval)delay
{
    //    SCNVector4 noRotation = SCNVector4Make(0, 1, 0, 0);
    CABasicAnimation *rotateChart = [CABasicAnimation animationWithKeyPath:@"rotation.w"];
    
    rotateChart.fromValue = @0;//[NSValue valueWithSCNVector4:noRotation];
    rotateChart.duration = 1.5;
    rotateChart.beginTime = CACurrentMediaTime() + 1.0;
    rotateChart.fillMode = kCAFillModeBackwards;
    rotateChart.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    return rotateChart;
}

- (CAAnimation *)reverseRotationAnimationWithDelay:(NSTimeInterval)delay forNode:(SCNNode *)node
{
    //    SCNVector4 noRotation = SCNVector4Make(0, 1, 0, 0);
    CABasicAnimation *rotateChartAnimation = [CABasicAnimation animationWithKeyPath:@"rotation.w"];
    rotateChartAnimation.fromValue = @(node.rotation.w);
    rotateChartAnimation.toValue = @0;
    rotateChartAnimation.duration = 1;
    rotateChartAnimation.beginTime = CACurrentMediaTime() + delay;
    rotateChartAnimation.fillMode = kCAFillModeBackwards;
    rotateChartAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    node.rotation = SCNVector4Make(node.rotation.x, node.rotation.y, node.rotation.z, 0);
    [node addAnimation:rotateChartAnimation forKey:@"rotation animation"];
    return rotateChartAnimation;
}

- (void)generateNewNumbers
{
    CGFloat min = 0;
    CGFloat max = 10;
    NSMutableArray *randomNumbers = [[NSMutableArray alloc] initWithCapacity:10];
    for (int i = 0; i < 10; i++) {
        CGFloat randomValue = floorf(((double)arc4random() / ARC4RANDOM_MAX) * (max - min) + min);
        [randomNumbers addObject:@(randomValue)];
    }
    self.numbers = randomNumbers;
}

@end
