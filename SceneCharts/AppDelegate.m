//
//  AppDelegate.m
//  SceneCharts
//
//  Created by Brandon Levasseur on 2/13/15.
//  Copyright (c) 2015 TheCodingArt. All rights reserved.
//

#import "AppDelegate.h"
#import "ChartView.h"

@interface AppDelegate ()

@property (weak) IBOutlet ChartView *chartView;

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
