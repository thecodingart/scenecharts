//
//  PieChartView.m
//  SceneCharts
//
//  Created by Brandon Levasseur on 2/14/15.
//  Copyright (c) 2015 TheCodingArt. All rights reserved.
//

#import "PieChartView.h"

@interface PieChartView ()

@property (weak, nonatomic) SCNNode *chartNode;

@end

@implementation PieChartView

- (void)awakeFromNib
{
    SCNScene *scene = [SCNScene scene];
    self.scene = scene;
    
    SCNNode *cameraNode = [SCNNode node];
    cameraNode.camera = [SCNCamera camera];
    cameraNode.position = SCNVector3Make(0, 0, 30);
    [scene.rootNode addChildNode:cameraNode];
    
    self.allowsCameraControl = YES;
    
self.numbers = @[@1,@2,@3,@4];
}

- (void)setNumbers:(NSArray *)numbers
{
    [self.chartNode removeFromParentNode];
    
    SCNNode *chartNode = [SCNNode node];
    [self.scene.rootNode addChildNode:chartNode];
    chartNode.position = SCNVector3Make(0, 0.25, 0);
    chartNode.rotation = SCNVector4Make(0, 1, 0, -M_PI/8.3);
    self.chartNode = chartNode;
    
    CGFloat sum = [[numbers valueForKeyPath:@"@sum.self"] doubleValue];
    NSPoint center = NSMakePoint(0, 0);
    CGFloat radius = 12.0;
    
    __block CGFloat startAngle = 0.0;
    
    [numbers enumerateObjectsUsingBlock:^(NSNumber *number, NSUInteger idx, BOOL *stop) {
        CGFloat segmentAngle = 360 * number.doubleValue / sum; //Calculates a percentage of 360 degrees
        CGFloat endAngle = startAngle + segmentAngle;
        
        NSBezierPath *segmentPath = [NSBezierPath bezierPath];
        [segmentPath moveToPoint:center];
        [segmentPath appendBezierPathWithArcWithCenter:center radius:radius startAngle:startAngle endAngle:endAngle];
        
        [segmentPath closePath];
        segmentPath.flatness = 0.05;
        
        CGFloat shapeThickness = 3.0;
        SCNShape *segmentShape = [SCNShape shapeWithPath:segmentPath extrusionDepth:shapeThickness];
        segmentShape.chamferRadius = 0.3;
        SCNNode *segmentNode = [SCNNode nodeWithGeometry:segmentShape];
        [chartNode addChildNode:segmentNode];
        
        //draw attention to one of the shapes by moving it away from the center of the pie chart
        if (idx == 1) {
            CGFloat midAngle = (startAngle + endAngle) / 2.0;
            midAngle = midAngle * M_PI / 180.0; // to radians
            CGFloat explodeDistance = 2.5;
            segmentNode.position = SCNVector3Make(explodeDistance * cos(midAngle), explodeDistance * sin(midAngle), 0.0);
        }
        
        //give each segment a unique color
        CGFloat hue = idx * 1.0/numbers.count;
        NSColor *color = [NSColor colorWithCalibratedHue:hue saturation:1.0 brightness:1.0 alpha:1.0];
        
        segmentShape.firstMaterial.diffuse.contents  = color;
        segmentShape.firstMaterial.specular.contents = [NSColor darkGrayColor];
        
        // increment the start angle for the next iteration
        startAngle = endAngle;
    }];
}

@end
