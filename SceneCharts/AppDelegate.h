//
//  AppDelegate.h
//  SceneCharts
//
//  Created by Brandon Levasseur on 2/13/15.
//  Copyright (c) 2015 TheCodingArt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

