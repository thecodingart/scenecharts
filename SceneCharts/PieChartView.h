//
//  PieChartView.h
//  SceneCharts
//
//  Created by Brandon Levasseur on 2/14/15.
//  Copyright (c) 2015 TheCodingArt. All rights reserved.
//

#import <SceneKit/SceneKit.h>

@interface PieChartView : SCNView

@property (copy, nonatomic) NSArray *numbers;

@end
